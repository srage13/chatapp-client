const initialAuthState = {
    userId: null,
    userToken: null,
    signupEmail: '',
    signupPassword: '',
    signupUsername: '',
    loginUsername: '',
    loginPassword: '',
    loginError: false,
    registerError: false,
    loginErrorMessage: null,
    registerErrorMessage: null,
  }
  

export default (state = initialAuthState, action) => {
    console.log(action.payload);
    switch (action.type) {
        case 'UPDATE_SIGNUP_FORM_FIELDS':
        case 'UPDATE_LOGIN_FORM_FIELDS':
            return { ...state, [action.payload.stateKey]: action.payload.stateValue }
        case 'REGISTRATION_SUCCESSFULL':
        case 'LOGIN_SUCCESSFULL':
            return { ...state, userId: action.payload.userId, userToken: action.payload.userToken}
        case 'REGISTRATION_ERROR':
            return { ...state, registerError: action.payload.error, registerErrorMessage: action.payload.message }
        case 'LOGIN_ERROR':
            return { ...state, loginError: action.payload.error, loginErrorMessage: action.payload.message }
        case 'CLEAR_ERROR':
            return { ...state, loginError: false, loginErrorMessage: null, registerError: false, registerErrorMessage: null }
        case 'LOGOUT':
            return { ...state, userId: null, userToken: null}
        default:
            return state;
    }
};
