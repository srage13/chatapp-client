import React from 'react';
import { BrowserRouter as Router, Route, Switch, Link, NavLink } from 'react-router-dom';
import history from './History';
import LoginPage from '../components/LoginPage';
import SignupPage from '../components/SignupPage';
import ChatPage from '../components/ChatPage';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';

const AppRouter = () => (
  <Router history={history}>
    <div>
      <Switch>
        <PublicRoute path="/" component={LoginPage} exact={true} />
        <PublicRoute path="/signup" component={SignupPage} />
        <PrivateRoute path="/chat" component={ChatPage} />
      </Switch>
    </div>
  </Router>
);

export default AppRouter;
