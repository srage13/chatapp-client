import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({
  component: Component,
  ...rest
}) => (
    <Route {...rest} component={(props) => 
      console.log(props)
      // (
      //   props.auth.userId != null && props.auth.userToken != null ? 
      //   (
      //     <div>
      //       <Component {...props} />
      //     </div>
      //   ) 
      //   : 
      //   (
      //     <Redirect to="/" />
      //   )
      // )
    } />
);

const mapStateToProps = state => {
  return {
      auth: state.auth
  }
}

export default connect(mapStateToProps)(PrivateRoute);
