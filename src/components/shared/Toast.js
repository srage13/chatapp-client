import React from 'react'
import { ToastContainer, toast } from 'react-toastify';
toast.configure({
	position: "bottom-center",
    autoClose: 4000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
});

export default function Toast() {
    return (
        <ToastContainer />
    )
}
