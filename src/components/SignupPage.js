import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { updateSignupFormFields, submitSignupForm, clearErrors } from '../actions/auth';
import { ToastContainer, toast } from 'react-toastify';
import Toast from './shared/Toast';


class SignupPage extends Component {

    constructor(props) {
        super(props);
        this.submitForm = this.submitForm.bind(this);
    }

    submitForm = (e) => {
        e.preventDefault();
        this.props.submitSignupForm();
    }

    componentWillReceiveProps(nextProps) {
        const { registerError, registerErrorMessage } = nextProps.auth
        if (registerError) {
            toast.error(registerErrorMessage, {
                position: "bottom-center",
                autoClose: 4000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                onClose: props => this.props.clearErrors()
            }) 
        }
    }

    render() {
        const { auth } = this.props;
        if(auth.userId != null && auth.userToken != null) return (<Redirect to='/chat'/>);
        return (
            <div>
                {auth.registerError ? <ToastContainer /> : null}
                <div className="account-pages my-5 pt-sm-5">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-md-8 col-lg-6 col-xl-5">
                                <div className="card overflow-hidden">
                                    <div className="bg-soft-primary">
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="text-primary set-center p-4">
                                                    <h5 className="text-primary">Register</h5>
                                                    <p>Register to get your chat account created.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body pt-0">
                                        <div className="p-2">
                                            <form className="form-horizontal" action="">

                                                <div className="form-group">
                                                    <label htmlFor="useremail">Email</label>
                                                    <input type="email" className="form-control" id="signupEmail" placeholder="Enter email" onChange = {(e) => this.props.updateSignupFormFields(e)}/>
                                                </div>

                                                <div className="form-group">
                                                    <label htmlFor="username">Username</label>
                                                    <input type="text" className="form-control" id="signupUsername" placeholder="Enter username"  onChange = {(e) => this.props.updateSignupFormFields(e)} />
                                                </div>

                                                <div className="form-group">
                                                    <label htmlFor="userpassword">Password</label>
                                                    <input type="password" className="form-control" id="signupPassword" placeholder="Enter password"  onChange = {(e) => this.props.updateSignupFormFields(e)} />
                                                </div>

                                                <div className="mt-4">
                                                    <button className="btn btn-primary btn-block waves-effect waves-light" type="submit" onClick = {(e) => this.submitForm(e)}>Register</button>
                                                </div>

                                            </form>
                                        </div>

                                    </div>
                                </div>
                                <div className="mt-5 text-center">
                                    <p>Already have an account ? <Link to="/" className="font-weight-medium text-primary" >Login</Link></p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = (dispatch) => ({
    updateSignupFormFields: (e) => dispatch(updateSignupFormFields(e)),
    submitSignupForm: (e) => dispatch(submitSignupForm(e)),
    clearErrors: () => dispatch(clearErrors())
});

export default connect(mapStateToProps, mapDispatchToProps)(SignupPage);