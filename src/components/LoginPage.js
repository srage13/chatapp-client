import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { updateLoginFormFields, submitLoginForm, clearErrors } from '../actions/auth';
import { ToastContainer, toast } from 'react-toastify';
import Toast from './shared/Toast';


class LoginPage extends Component {

    constructor(props) {
        super(props);
        this.submitForm = this.submitForm.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { loginError, loginErrorMessage } = nextProps.auth
        if (loginError) {
            toast.error(loginErrorMessage, {
                position: "bottom-center",
                autoClose: 4000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                onClose: props => this.props.clearErrors()
            }) 
        }
    }

    submitForm = (e) => {
        e.preventDefault();
        this.props.submitLoginForm();
    }

    render() {
        const { auth } = this.props;
        if (auth.userId != null && auth.userToken != null) return (<Redirect to='/chat' />);
        return (
            <div>
                {auth.loginError ? <ToastContainer /> : null}
                <div className="account-pages my-5 pt-sm-5">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-md-8 col-lg-6 col-xl-5">
                                <div className="card overflow-hidden">
                                    <div className="bg-soft-primary">
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="text-primary set-center p-4">
                                                    <h5 className="text-primary">Chat Application</h5>
                                                    <p>Sign in to continue.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body pt-0">

                                        <div className="p-2">
                                            <form className="form-horizontal" >

                                                <div className="form-group">
                                                    <label htmlFor="username">Username</label>
                                                    <input type="text" className="form-control" id="loginUsername" placeholder="Enter username" onChange={(e) => this.props.updateLoginFormFields(e)} />
                                                </div>

                                                <div className="form-group">
                                                    <label htmlFor="userpassword">Password</label>
                                                    <input type="password" className="form-control" id="loginPassword" placeholder="Enter password" onChange={(e) => this.props.updateLoginFormFields(e)} />
                                                </div>

                                                <div className="mt-3">
                                                    <button className="btn btn-primary btn-block waves-effect waves-light" type="submit" onClick={(e) => this.submitForm(e)}>Log In</button>
                                                </div>

                                            </form>
                                        </div>

                                    </div>
                                </div>
                                <div className="mt-5 text-center">
                                    <p>Don't have an account ? <Link to="/signup" className="font-weight-medium text-primary" >Sign Up</Link> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = (dispatch) => ({
    updateLoginFormFields: (e) => dispatch(updateLoginFormFields(e)),
    submitLoginForm: (e) => dispatch(submitLoginForm(e)),
    clearErrors: (e) => dispatch(clearErrors())
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);