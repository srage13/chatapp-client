import axios from 'axios';

export const updateSignupFormFields = (e) => {
    return(dispatch) => {
        dispatch({
            type: 'UPDATE_SIGNUP_FORM_FIELDS',
            payload: {
                stateKey: e.target.id,
                stateValue: e.target.value
            } 
        });
    }
}

export const submitSignupForm = () => {
    return(dispatch, getState) => {
        axios({
            method: 'post',
            url: 'http://localhost:8080/api/user/register',
            data: {
                email: getState().auth.signupEmail,
                userName: getState().auth.signupUsername,
                password: getState().auth.signupPassword
            }
        })
        .then((res) => {
            if(res.data.success) {
                dispatch({
                    type: 'REGISTRATION_SUCCESSFULL',
                    payload: {
                        userId: res.data.userId,
                        userToken: res.data.accessToken
                    } 
                });
            } else {
                dispatch({
                    type: 'REGISTRATION_ERROR',
                    payload: {
                        error: !res.data.success,
                        message: res.data.errorMessage
                    } 
                });
            }

        })
        .catch((error) => {
            dispatch({
                type: 'REGISTRATION_ERROR',
                payload: {
                    error: false,
                    message: ""
                }
            });
        });
    }
    
}

export const updateLoginFormFields = (e) => {
    return(dispatch) => {
        dispatch({
            type: 'UPDATE_LOGIN_FORM_FIELDS',
            payload: {
                stateKey: e.target.id,
                stateValue: e.target.value
            } 
        });
    }
}

export const submitLoginForm = () => {
    return(dispatch, getState) => {
        axios({
            method: 'post',
            url: 'http://localhost:8080/api/user/login',
            data: {
                username: getState().auth.loginUsername,
                password: getState().auth.loginPassword
            }
        })
        .then((res) => {
            console.log(res.data);
            if(res.data.success) {
                dispatch({
                    type: 'LOGIN_SUCCESSFULL',
                    payload: {
                        userId: res.data.userId,
                        userToken: res.data.accessToken
                    } 
                });
            } else {
                dispatch({
                    type: 'LOGIN_ERROR',
                    payload: {
                        error: !res.data.success,
                        message: res.data.errorMessage
                    } 
                });
            }

        })
        .catch((error) => {
            console.log(error);
        });
    }
}

export const clearErrors = () => {
    return(dispatch) => {
        dispatch({
            type: 'CLEAR_ERROR'
        });
    }
}