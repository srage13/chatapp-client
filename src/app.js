import React from 'react';
import ReactDOM from 'react-dom';
import LoginPage from './components/LoginPage';
import SignupPage from './components/SignupPage';
import reduxStore from './redux-store/ReduxStore';
import { Provider } from 'react-redux';
import AppRouter from './router/AppRouter';

const store = reduxStore();

const jsx = (
    <Provider store={store}>
        <AppRouter />
    </Provider>
);

ReactDOM.render(jsx, document.getElementById('app'));

// ReactDOM.render(<SignupPage />, document.getElementById('app'));